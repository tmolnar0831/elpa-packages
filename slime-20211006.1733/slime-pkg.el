(define-package "slime" "20211006.1733" "Superior Lisp Interaction Mode for Emacs"
  '((cl-lib "0.5")
    (macrostep "0.9"))
  :commit "0470fc048fbd7987a25413be37f4e0efd93c204f" :keywords
  '("languages" "lisp" "slime")
  :url "https://github.com/slime/slime")
;; Local Variables:
;; no-byte-compile: t
;; End:
